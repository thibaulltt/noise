#include "../include/fullscreenimagewidget.hpp"

#include <QMouseEvent>
#include <QVBoxLayout>
#include <QtDebug>

FullscreenImageWidget::FullscreenImageWidget(const image &img, QWidget *parent):
	QDialog(parent)
{
	m_imageWidget = new ImageWidget(img);
	m_imageWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	m_imageWidget->setFullScreenPopupOnClick(false);
	m_closeButton = new QPushButton("Fermer");
	m_closeButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

	connect(m_closeButton, SIGNAL(clicked()), this, SLOT(close()));

	QVBoxLayout *layout(new QVBoxLayout(this));
	layout->addWidget(m_imageWidget, 3);
	layout->addWidget(m_closeButton, 0, Qt::AlignCenter);
	this->setLayout(layout);

	setAttribute(Qt::WA_DeleteOnClose);
}

int FullscreenImageWidget::exec() {
	showFullScreen();
	return QDialog::Accepted;
}
