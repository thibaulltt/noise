#include "../include/figurewidget.hpp"

#include <QVBoxLayout>

FigureWidget::FigureWidget(const QString &description,
			   ImageWidget *imageWidget,
			   QWidget *parent): QWidget(parent)
{
	m_description = new QLabel(description, this);
	if(imageWidget) {
		m_imageWidget = imageWidget;
	}
	else {
		m_imageWidget = new ImageWidget();
	}
	m_imageWidget->setParent(this);

	QVBoxLayout *layout = new QVBoxLayout(this);

    layout->addStretch(1);
    layout->addWidget(m_description, 1, Qt::AlignCenter);
    layout->addWidget(m_imageWidget, 10);
    layout->addStretch(1);
}

void FigureWidget::setImage(const image& image) {
	m_imageWidget->setImage(image);
}

const image &FigureWidget::getImage() const {
	return m_imageWidget->getImage();
}
