#include "../include/mainwindow.hpp"
#include "../include/noisewidget.hpp"
#include "../include/plotwidget.hpp"
#include "../include/image_storage.h"

#include <QApplication>
#include <QtDebug>
#include <QTextEdit>

image_database imageData;

int main(int argv, char** args) {
	QApplication app(argv, args);

	MainWindow window;
	window.show();

	return app.exec();
}
