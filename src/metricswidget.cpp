#include "../include/metricswidget.hpp"
#include "../include/moc_metricswidget.cpp"

#include "../include/imagewidget.hpp"
#include "../include/applicationdata.h"

#include <QGridLayout>
#include <QShortcut>
#include <QtDebug>

MetricsWidget::MetricsWidget(QWidget *parent) :
	QWidget(parent)
{
	QGridLayout *layout(new QGridLayout(this));
	setLayout(layout);

	m_psnrLabel = new QLabel("PSNR : ", this);
	m_psnrLabel->setAlignment(Qt::AlignRight);
	m_psnrValue = new QLabel("?", this);
	m_psnrValue->setAlignment(Qt::AlignLeft);

	m_noiseModelLabel = new QLabel("Noise model : ", this);
	m_noiseModelLabel->setAlignment(Qt::AlignRight);
	QVector<nDis_t> noiseValues = imageData.getNoiseModel();
	QString muString = "";
	QString sigmaString = "";
	if (noiseValues.size() != 0) {
		if (noiseValues.size() == 1) {
			muString = "µ = " + QString::number(noiseValues[0].mu);
			sigmaString = "σ = " + QString::number(noiseValues[0].sigma);
		} else {
			Q_ASSERT(noiseValues.size() == 3);
			muString = QString::number(noiseValues[0].mu) + "," + QString::number(noiseValues[1].mu) + "," + QString::number(noiseValues[2].mu);
			sigmaString = QString::number(noiseValues[0].sigma) + "," + QString::number(noiseValues[1].sigma) + "," + QString::number(noiseValues[2].sigma);
		}
	}
	m_muValue = new QLabel(muString, this);
	m_sigmaValue = new QLabel(sigmaString, this);
	m_muValue->setAlignment(Qt::AlignLeft);
	m_sigmaValue->setAlignment(Qt::AlignLeft);

	layout->addWidget(m_psnrLabel, 0, 0);
	layout->addWidget(m_psnrValue, 0, 1);
	layout->addWidget(m_noiseModelLabel, 1, 0);
	layout->addWidget(m_muValue, 1, 1);
	layout->addWidget(m_sigmaValue, 2, 1);

	this->setWindowTitle("Qt : Metrics");

	QShortcut *shortcut = new QShortcut(QKeySequence(Qt::Key::Key_Escape), this, SLOT(closeAsked()));
	Q_UNUSED(shortcut)

	setAttribute(Qt::WA_DeleteOnClose);
	setMinimumSize(200, 25);

	connect(ApplicationData::get(), SIGNAL(workImageChanged()), this, SLOT(onWorkImageChanged()));
	connect(ApplicationData::get(), SIGNAL(groundTruthChanged()), this, SLOT(onGroundTruthChanged()));
	recomputeMetrics();
}

void MetricsWidget::closeAsked() {
	close();
}

void MetricsWidget::onWorkImageChanged() {
	recomputeMetrics();
}

void MetricsWidget::onGroundTruthChanged() {
	recomputeMetrics();
}

void MetricsWidget::recomputeMetrics() {
	ApplicationData *appData(ApplicationData::get());
	const image &groundTruth(appData->groundTruth());
	const image &workImage(appData->workImage());
	m_psnrValue->setText(QString::number(static_cast<double>(workImage.PSNR(groundTruth))));
}
