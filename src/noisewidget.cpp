#include "../include/noisewidget.hpp"
#include "../include/moc_noisewidget.cpp"

#include "../include/imagewidget.hpp"
#include "../include/applicationdata.h"

#include <QVBoxLayout>
#include <QShortcut>
#include <QtDebug>

#include <cmath>

NoiseWidget::NoiseWidget(const image &baseImage, MainWindow *mainWindow, QWidget *parent) :
			 QWidget(parent), m_baseImage(baseImage),
			 m_mainWindow(mainWindow), m_factor(1.f)
{
	QVBoxLayout *layout(new QVBoxLayout(this));
	setLayout(layout);

	m_imageWidget = new FigureWidget("Image bruitée",
					new ImageWidget(m_baseImage),
					this);

	m_slider = new QSlider(Qt::Horizontal, this);
	m_slider->setRange(0, 100);
	m_slider->setValue(100);
	connect(m_slider, SIGNAL(valueChanged(int)), this, SLOT(setFactor(int)));

	m_validateButton = new QPushButton("Valider", this);
	connect(m_validateButton, SIGNAL(clicked()), this, SLOT(noiseImage()));

	this->setWindowTitle("Qt : Modèle de bruit");

	layout->addWidget(m_imageWidget);
	layout->addWidget(m_slider);
	layout->addWidget(m_validateButton);

	QShortcut *shortcut = new QShortcut(QKeySequence(Qt::Key::Key_Escape), this, SLOT(closeAsked()));
	Q_UNUSED(shortcut)

	setAttribute(Qt::WA_DeleteOnClose);

	connect(ApplicationData::get(), SIGNAL(noiseValuesChanged()), this, SLOT(onNoiseValuesChange()));
}

void NoiseWidget::closeAsked() {
	close();
}

void NoiseWidget::setFactor(int factor) {
	m_factor = static_cast<float>(factor) / 100.f;
}

void NoiseWidget::noiseImage() {
	const QVector<QVector<int>> &noiseValues(ApplicationData::get()->noiseValues());
	bool color = noiseValues.size() == 3;
	bool grayscale = noiseValues.size() == 1;
	if(!(color || grayscale)) {
		qWarning() << "Cannot apply noise model, it is invalid.";
	}
	QVector<nDis_t> gaussDist = imageData.getNoiseModel();

	if (gaussDist.size() == 0) {
		return;
	}

	image resImage(m_baseImage);
	int nbpixs = resImage.width() * resImage.height();
	for(int i(0); i < nbpixs; ++i) {
		int x(i % resImage.width());
		int y(i / resImage.width());
		QColor pix(resImage.at(x, y));

		double r, g, b;
		if(grayscale) {
			r = generateRandomGaussianValue(gaussDist[0]);
			b = g = r;
		}
		else {
			r = generateRandomGaussianValue(gaussDist[0]);
			g = generateRandomGaussianValue(gaussDist[1]);
			b = generateRandomGaussianValue(gaussDist[2]);
		}

		int ir, ig, ib;
		ir = qBound(0, pix.red() + qRound(r), 255);
		ig = qBound(0, pix.green() + qRound(g), 255);
		ib = qBound(0, pix.blue() + qRound(b), 255);

		resImage.setPixelColor(x, y, QColor(ir, ig, ib));
	}

	m_imageWidget->setImage(resImage);
	ApplicationData::get()->setWorkImage(resImage);
}

void NoiseWidget::onNoiseValuesChange() {
	if(m_factor != 0.0f) {
		noiseImage();
	}
}
