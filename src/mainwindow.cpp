#include "../include/mainwindow.hpp"
#include "../include/moc_mainwindow.cpp"

#include "../include/noisewidget.hpp"
#include "../include/denoisewidget.hpp"
#include "../include/noisemodelwidget.hpp"
#include "../include/metricswidget.hpp"
#include "../include/applicationdata.h"
#include "../include/image_storage.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QToolBar>
#include <QtDebug>
#include <QFileDialog>
#include <QShortcut>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent, Qt::WindowFlags flags):
	QMainWindow(parent, flags)
{
	m_centralWidget = new QWidget(this);
	QHBoxLayout *centralLayout(new QHBoxLayout(m_centralWidget));
	m_centralWidget->setLayout(centralLayout);
	setCentralWidget(m_centralWidget);
	m_centralWidget->show();

	m_groundTruth = new FigureWidget("Vérité de terrain",
					 new ImageWidget(image(":/assets/placeholder.png")),
					 m_centralWidget);
	m_image = new FigureWidget("Image de travail",
				   new ImageWidget(image(":/assets/placeholder.png")),
				   m_centralWidget);

	//    centralLayout->addWidget(m_groundTruth, 0, Qt::AlignVCenter);
	//    centralLayout->addWidget(m_image, 0, Qt::AlignVCenter);
	centralLayout->addWidget(m_groundTruth, 1);
	centralLayout->addWidget(m_image, 1);

	m_buttonsWidget = new QWidget(this);
	QVBoxLayout *buttonLayout = new QVBoxLayout(m_buttonsWidget);
	m_buttonsWidget->setLayout(buttonLayout);

	m_noiseModelButton = new QPushButton("Modèle de bruit", m_buttonsWidget);
	connect(m_noiseModelButton, SIGNAL(clicked()), this, SLOT(openNoiseModelWidget()));
	m_noiseImageButton = new QPushButton("Bruiter", m_buttonsWidget);
	connect(m_noiseImageButton, SIGNAL(clicked()), this, SLOT(openNoiseWidget()));

	m_denoiseImageButton = new QPushButton("Débruiter", m_buttonsWidget);
	connect(m_denoiseImageButton, SIGNAL(clicked()), this, SLOT(openDenoiseWidget()));

	m_metricsButton = new QPushButton("Accéder aux métriques", m_buttonsWidget);
	connect(m_metricsButton, SIGNAL(clicked()), this, SLOT(openMetricsWidget()));

	m_resetButton = new QPushButton("Rétablir la vérité de terrain", m_buttonsWidget);
	connect(m_resetButton, SIGNAL(clicked()), this, SLOT(resetWorkImage()));

	buttonLayout->addWidget(m_noiseModelButton);
	buttonLayout->addWidget(m_noiseImageButton);
	buttonLayout->addWidget(m_denoiseImageButton);
	buttonLayout->addWidget(m_metricsButton);
	buttonLayout->addWidget(m_resetButton);

	centralLayout->addWidget(m_buttonsWidget);

	// BUTTON CONNECTION TODO

	QToolBar *toolbar = new QToolBar(this);
	toolbar->addAction(QIcon(), "Choisir l'image de base", this, &MainWindow::openImage);
	toolbar->addAction(QIcon(), "Enregistrer l'image de travail", this, &MainWindow::saveImage);

	addToolBar(toolbar);

	this->setWindowTitle("Qt : Image viewer");

	QShortcut *shortcut = new QShortcut(QKeySequence(Qt::Key::Key_Escape), this, SLOT(closeAsked()));
	Q_UNUSED(shortcut)

	connect(ApplicationData::get(), SIGNAL(groundTruthChanged()), this, SLOT(onGroundTruthChanged()));
	connect(ApplicationData::get(), SIGNAL(workImageChanged()), this, SLOT(onWorkImageChanged()));

	image img(":/img/01.pgm");
	ApplicationData::get()->setGroundTruth(img);
	ApplicationData::get()->setWorkImage(img);
}

MainWindow::~MainWindow() {

}

void MainWindow::closeEvent(QCloseEvent *ev) {
	emit closeAll();
	QMainWindow::closeEvent(ev);
}

void MainWindow::openImage() {
	QString filename = QFileDialog::getOpenFileName(this,
							"Sélectionner l'image de base",
							QString(),
							tr("Image Files (*.png *.jpg *.bmp *.ppm *.pgm)"));
	if(!filename.isNull()) {
		image img(filename.toStdString());
		ApplicationData::get()->setGroundTruth(img);
		ApplicationData::get()->setWorkImage(img);
	}
}

void MainWindow::saveImage() {
	QString filename = QFileDialog::getSaveFileName(this,
							"Sélectionner l'image de base",
							QString(),
							tr("Image Files (*.png *.jpg *.bmp *.ppm *.pgm)"));
	if(!filename.isNull()) {
		if(!ApplicationData::get()->workImage().save(filename)) {
			QMessageBox dialog(QMessageBox::Icon::Warning,
					   "Cannot save the image",
					   "The image cannot be saved "
					   "to "+filename+".",
					   QMessageBox::StandardButton::Ok,
					   this);
			dialog.exec();
		}
	}
}

void MainWindow::finishedNoiseModel() {
	imageData.computeNoiseModel();
}

void MainWindow::openNoiseWidget() {
	NoiseWidget *noiseWidget = new NoiseWidget(m_image->getImage(), this, nullptr);
	connect(this, SIGNAL(closeAll()), noiseWidget, SLOT(closeAsked()));
	noiseWidget->show();
}

void MainWindow::openDenoiseWidget() {
	DenoiseWidget *denoiseWidget = new DenoiseWidget(m_image->getImage(), this, nullptr);
	connect(this, SIGNAL(closeAll()), denoiseWidget, SLOT(closeAsked()));
	denoiseWidget->show();
}

void MainWindow::openNoiseModelWidget() {
	NoiseModelWidget *noiseModelWidget = new NoiseModelWidget(this, nullptr);
	connect(this, SIGNAL(closeAll()), noiseModelWidget, SLOT(closeAsked()));
	connect(noiseModelWidget, SIGNAL(destroyed(QObject*)), this, SLOT(finishedNoiseModel()));
	noiseModelWidget->show();
}

void MainWindow::openMetricsWidget() {
	MetricsWidget *metricsWidget = new MetricsWidget(nullptr);
	connect(this, SIGNAL(closeAll()), metricsWidget, SLOT(closeAsked()));
	metricsWidget->show();
}

void MainWindow::resetWorkImage() {
	ApplicationData::get()->setWorkImage(m_groundTruth->getImage());
}

void MainWindow::closeAsked() {
	emit closeAll();
	close();
}

void MainWindow::onGroundTruthChanged() {
	m_groundTruth->setImage(ApplicationData::get()->groundTruth());
}

void MainWindow::onWorkImageChanged() {
	m_image->setImage(ApplicationData::get()->workImage());
}
