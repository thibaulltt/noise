#include "../include/imagewidget.hpp"
#include "../include/fullscreenimagewidget.hpp"
#include "../include/image.hpp"

#include <QVBoxLayout>
#include <QtDebug>
#include <QMouseEvent>

ImageWidget::ImageWidget(QWidget *parent): QLabel(parent)
{
	init();
	clearImage();
}

ImageWidget::ImageWidget(const image &base_image, QWidget *parent):
	QLabel(parent)
{
	init();
	setImage(base_image);
}

ImageWidget::~ImageWidget() {

}

void ImageWidget::setImage(const image &base_image) {
	m_image = base_image;
	m_pixmap = QPixmap::fromImage(m_image);
	setPixmap(m_pixmap.scaled(width(), height(), Qt::KeepAspectRatio));
}

void ImageWidget::clearImage() {
	setImage(image(":/assets/placeholder.png"));
}

const image &ImageWidget::getImage() const {
	return m_image;
}

void ImageWidget::setFullScreenPopupOnClick(bool val) {
	m_fullScreenPopupOnClick = val;
	if(m_pressed && !m_fullScreenPopupOnClick) {
		m_pressed = false;
	}
}

void ImageWidget::init() {
	setAlignment(Qt::AlignHCenter);
	setMinimumSize(100, 100);
}

void ImageWidget::mouseReleaseEvent(QMouseEvent *ev) {
	if(m_pressed) {
		QPoint mousePos(ev->pos());
		if(0 <= mousePos.x() && mousePos.x() < width() &&
				0 <= mousePos.y() && mousePos.y() < height()) {
			clickedFunction();
		}

		m_pressed = false;
	}
}

void ImageWidget::mousePressEvent(QMouseEvent *ev) {
	Q_UNUSED(ev)
	if(m_fullScreenPopupOnClick) {
		m_pressed = true;
	}
}

void ImageWidget::resizeEvent(QResizeEvent *event) {
	QLabel::resizeEvent(event);
	setPixmap(m_pixmap.scaled(width(), height(), Qt::KeepAspectRatio));
}

void ImageWidget::clickedFunction() {
	// Show fullscreen widget
	FullscreenImageWidget *dialog(new FullscreenImageWidget(m_image, this));
	dialog->exec();
}
