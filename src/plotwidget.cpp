#include "../include/plotwidget.hpp"
#include "../include/moc_plotwidget.cpp"

PlotWidget::PlotWidget(const QVector<QVector<QPointF>> &seriess, const QString& title, QWidget *parent):
	QChartView(parent)
{
	m_chart = new QChart();

	m_chart->createDefaultAxes();
	m_chart->setTitle(title);

	for(const QVector<QPointF> &seriesData: seriess) {
		QLineSeries *series = m_series[addSerie()];
		for(const QPointF &data: seriesData) {
			series->append(data);
		}
	}

	for(QLineSeries *series: m_series) {
		m_chart->addSeries(series);
	}

	setRenderHint(QPainter::Antialiasing);

	this->setWindowTitle("Qt : Image noise plot");
}

PlotWidget::~PlotWidget() {
	delete m_chart;
}

int PlotWidget::seriesCount() const {
	return m_series.count();
}

int PlotWidget::addSerie(const QString &name) {
	m_series.push_back(new QLineSeries());
	m_series.back()->setName(name);
	m_chart->addSeries(m_series.back());
	return m_series.count() - 1;
}

int PlotWidget::addSerie(const QString &name, const QColor &color) {
	m_series.push_back(new QLineSeries());
	m_series.back()->setName(name);
	m_series.back()->setColor(color);
	m_chart->addSeries(m_series.back());
	return m_series.count() - 1;
}

void PlotWidget::addPointToSerie(int serieIndex, const QPointF &point) {
	Q_ASSERT(serieIndex < m_series.count());
	m_series[serieIndex]->append(point);
	if(serieIndex == m_series.count() - 1) {
		m_chart->removeSeries(m_series[serieIndex]);
		m_chart->addSeries(m_series[serieIndex]);
	}
	else {
		for(QLineSeries *series: m_series) {
			m_chart->removeSeries(series);
		}
		for(QLineSeries *series: m_series) {
			m_chart->addSeries(series);
		}
	}
	m_chart->createDefaultAxes();
	setChart(m_chart);
}

void PlotWidget::plotVector(QVector<QPointF> &series, QString name, const QColor &color) {
	int series_id = this->addSerie(name, color);
	for (int i = 0; i < series.size(); ++i) {
		this->addPointToSerie(series_id, series[i]);
	}
}

template <typename T> void PlotWidget::plotVector(QVector<T> &series, QString name, const QColor &color) {
	int series_id = this->addSerie(name, color);
	for (int i = 0; i < series.size(); ++i) {
		this->addPointToSerie(series_id, QPointF(static_cast<qreal>(i), static_cast<qreal>(series[i])));
	}
}

void PlotWidget::clear() {
	m_chart->removeAllSeries();
	m_series.clear();
}
