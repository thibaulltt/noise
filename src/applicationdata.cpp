#include "../include/applicationdata.h"

#include "../include/moc_applicationdata.cpp"

ApplicationData *ApplicationData::get() {
	static ApplicationData instance;
	return &instance;
}

void ApplicationData::setNoiseValues(const QVector<QVector<int>> &noiseValues) {
	m_noiseValues = noiseValues;
	emit noiseValuesChanged();
}

const QVector<QVector<int>> &ApplicationData::noiseValues() const {
	return m_noiseValues;
}

void ApplicationData::setWorkImage(const image &img) {
	m_workImage = img;
	emit workImageChanged();
}

const image &ApplicationData::workImage() const {
	return m_workImage;
}

void ApplicationData::setGroundTruth(const image &img) {
    m_groundTruth = img;
    emit groundTruthChanged();
}

const image &ApplicationData::groundTruth() const {
    return m_groundTruth;
}

ApplicationData::ApplicationData() {

}
