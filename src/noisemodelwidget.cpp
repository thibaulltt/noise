#include "../include/noisemodelwidget.hpp"
#include "../include/moc_noisemodelwidget.cpp"
#include "../include/image_storage.h"

#include "../include/applicationdata.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QShortcut>
#include <QtDebug>
#include <QtMath>
#include <QMessageBox>

#include <random>
#include <algorithm>

NoiseModelWidget::NoiseModelWidget(MainWindow *mainWindow, QWidget *parent) :
				  QWidget(parent), m_mainWindow(mainWindow) {

	setAttribute(Qt::WA_DeleteOnClose);
	this->setWindowTitle("Qt : Noise plot widget");

	QHBoxLayout *layout = new QHBoxLayout(this);
	setLayout(layout);

	// The left layout :
	m_leftPanel = new QWidget(this);
	QVBoxLayout *leftPanelLayout = new QVBoxLayout(m_leftPanel);
	m_leftPanel->setLayout(leftPanelLayout);
	layout->addWidget(m_leftPanel);

	// The center layout :
	m_centerPanel = new QWidget(this);
	QVBoxLayout *centerPanelLayout = new QVBoxLayout(m_centerPanel);
	m_centerPanel->setLayout(centerPanelLayout);
	layout->addWidget(m_centerPanel);

	// The center layout :
	m_rightPanel = new QWidget(this);
	QVBoxLayout *rightPanelLayout = new QVBoxLayout(m_rightPanel);
	m_rightPanel->setLayout(rightPanelLayout);
	layout->addWidget(m_rightPanel);

	// LEFT PANEL : LIST + 3 BUTTONS

	// The list of items
	m_openImages = new QListWidget(m_leftPanel);
	m_openImages->setSortingEnabled(true);
	m_openImages->setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(m_openImages, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), this, SLOT(onListClick(QListWidgetItem*, QListWidgetItem*)));
	leftPanelLayout->addWidget(m_openImages);

	// The button area (3 buttons)
	m_leftButtonArea = new QWidget(m_leftPanel);
	leftPanelLayout->addWidget(m_leftButtonArea);
	QHBoxLayout *leftButtonAreaLayout = new QHBoxLayout(m_leftButtonArea);
	m_leftButtonArea->setLayout(leftButtonAreaLayout);

	// The button to add a folder
	m_addImages = new QPushButton("Ajouter Dossiers", m_leftButtonArea);
	connect(m_addImages, SIGNAL(clicked()), this, SLOT(onAddFolderClicked()));
	leftButtonAreaLayout->addWidget(m_addImages);
	// The button to add a file
	m_addSingleImages = new QPushButton("Ajouter Fichiers", m_leftButtonArea);
	connect(m_addSingleImages, SIGNAL(clicked()), this, SLOT(onAddFileClicked()));
	leftButtonAreaLayout->addWidget(m_addSingleImages);
	// The button to remove a file
	m_removeImages = new QPushButton("Supprimer", m_leftButtonArea);
	connect(m_removeImages, SIGNAL(clicked()), this, SLOT(onRemoveImageClicked()));
	leftButtonAreaLayout->addWidget(m_removeImages);

	// The normal image viewer and noise image viewer
	m_regularImage = new FigureWidget("Image originelle", new ImageWidget(), m_centerPanel);
	m_noiseImage = new FigureWidget("Bruit", new ImageWidget(), m_centerPanel);
	m_centerPanel->layout()->addWidget(m_regularImage);
	m_centerPanel->layout()->addWidget(m_noiseImage);

	// The denoised image viewer and plot viewer
	m_denoisedImage = new FigureWidget("Image débruitée", new ImageWidget(), m_rightPanel);
	m_distribution = new PlotWidget({}, "Modèle du bruit", m_rightPanel);
	m_rightPanel->layout()->addWidget(m_denoisedImage);
	m_rightPanel->layout()->addWidget(m_distribution);

	QShortcut *shortcut = new QShortcut(QKeySequence(Qt::Key::Key_Escape), this, SLOT(closeAsked()));
	Q_UNUSED(shortcut)

	if (imageData.images.size() > 0) {
		for (const auto& img : imageData.images) {
			m_openImages->addItem(img.first);
		}
	}
}

void NoiseModelWidget::updatePlotAndImage(QString name) {
	if (this->m_openImages->count() == 0) {
		m_regularImage->setImage(image(":/assets/placeholder.png"));
		m_denoisedImage->setImage(image(":/assets/placeholder.png"));
		m_noiseImage->setImage(image(":/assets/placeholder.png"));
		m_distribution->clear();
		return;
	}
	auto pathToImageIterator = (name.size() == 0) ? imageData.images.begin() : ((imageData.contains(name) > 0) ? imageData.images.find(name) : imageData.images.begin());
	m_distribution->clear();
	int i(0);
	for(auto &histo: pathToImageIterator->second.histos) {
		m_distribution->addSerie("channel "+ QString::number(i));
		unsigned int x(0);
		for(const auto &pair: histo) {
			m_distribution->addPointToSerie(i, QPointF(pair.first, pair.second));
			++x;
		}
		++i;
	}

	m_regularImage->setImage(pathToImageIterator->second.originalImage);
	m_denoisedImage->setImage(pathToImageIterator->second.blurredImage);
	m_noiseImage->setImage(noiseValuesToImage(name, generateNoiseValues(name, pathToImageIterator->second.histos)));
}

QVector<QVector<int>> NoiseModelWidget::generateNoiseValues(const QString& name, const QVector<std::map<int, size_t>> &histos_sum) const {
	int channels(histos_sum.count());
	auto pathToImageIterator = (name.size() == 0) ? imageData.images.begin() : ((imageData.contains(name) > 0) ? imageData.images.find(name) : imageData.images.begin());
	int pixelNb(pathToImageIterator->second.originalImage.width() * pathToImageIterator->second.originalImage.height());

	QVector<QVector<int>> noiseValues(channels);

	int histo_i = 0;
	for(const auto &histo: histos_sum) {
		// Contruct the prob array
		for(const auto &valOcc: histo) {
			int val = valOcc.first;
			size_t occ = valOcc.second;
			if(occ == 0) {
				continue;
			}
			qreal nbValToInsertF(static_cast<qreal>(occ));
			size_t nbValToInsert(nbValToInsertF <= 0.0 ? 0 : static_cast<size_t>(qCeil(nbValToInsertF)));

			noiseValues[histo_i].append(QVector<int>(static_cast<int>(nbValToInsert), val));
		}

		std::random_device rd;
		std::mt19937 g(rd());
		std::shuffle(noiseValues[histo_i].begin(), noiseValues[histo_i].end(), g);
		noiseValues[histo_i].resize(pixelNb);

		++histo_i;
	}

	ApplicationData::get()->setNoiseValues(noiseValues);

	return noiseValues;
}

image NoiseModelWidget::noiseValuesToImage(const QString& name, const QVector<QVector<int> > &noiseValues) const {
	image *refImage;
	if (imageData.contains(name) > 0) {
		refImage = &(imageData.images.find(name)->second.originalImage);
	} else {
		refImage = &(imageData.images.begin()->second.originalImage);
	}
	bool color(noiseValues.size() == 3);
	bool grayscale(noiseValues.size() == 1);
	Q_ASSERT(grayscale || color);

	image res(*refImage);
	for(int i(0); i < noiseValues[0].size(); ++i) {
		int x(i % res.width());
		int y(i / res.width());

		int r, g, b;
		if(grayscale) {
			r = noiseValues[0][i];
			b = g = r;
		}
		else {
			r = noiseValues[0][i];
			g = noiseValues[1][i];
			b = noiseValues[2][i];
		}

		r = qBound(0, r + 128, 255);
		g = qBound(0, g + 128, 255);
		b = qBound(0, b + 128, 255);

		res.setPixelColor(x, y, QColor(r, g, b));
	}

	return res;
}

void NoiseModelWidget::onAddFileClicked() {
	QStringList filenames = QFileDialog::getOpenFileNames(this, "Sélectionner les images à ajouter pour le modèle de bruit",
								  QString(), tr("Image Files (*.png *.jpg *.bmp *.ppm *.pgm)"));

	this->onAddImageClicked(filenames);
}

void NoiseModelWidget::onAddFolderClicked() {
	QDir dir = QDir(QFileDialog::getExistingDirectory(this, tr("Open Directory"),
							QString(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
	QStringList filenames = dir.entryList(QStringList() << "*.ppm" << "*.pgm",QDir::Files);
	for (QString& filename : filenames) {
		filename = dir.absolutePath() + "/" + filename;
	}

	this->onAddImageClicked(filenames);
}

void NoiseModelWidget::onAddImageClicked(QStringList& filenames) {
	QStringList rejectedImages;
	bool insertion = false;

	for(const QString &filename: filenames) {
		QString shortName(QFileInfo(filename).baseName());
		// if the image is not already loaded
		if(imageData.images.count(shortName) == 0) {
			// open it
			insertion = true;
			// blur it
			std::string fn = filename.toStdString();
			image img(fn);
			image imgBlurred(img);
			imgBlurred.apply_gaussian_blur();
			// generate diff histogram
			QVector<std::map<int, size_t>> histos(img.diff_histo(imgBlurred));

			bool is_same_colorspace = true;
			// check if only one type (color/greyscale) of images are loaded
			if(!imageData.images.empty()) {
				int lastImageChannelNb = imageData.images.rbegin()->second.histos.count();
				int imageChannelNb = histos.count();
				// TODO : Add images, only if this is true ?
				is_same_colorspace = lastImageChannelNb == imageChannelNb;
			}
			if (is_same_colorspace) {
				m_openImages->addItem(shortName);
				imageData.addImage(shortName, img, imgBlurred, histos);
			} else {
				rejectedImages.push_back(shortName);
			}
		}
		else {
			rejectedImages.push_back(shortName);
		}
	}

	if(insertion) {
		updatePlotAndImage();
	}

	if(!rejectedImages.empty()) {
		QString str("");
		for(QString rejected: rejectedImages) {
			str += "- "+rejected+"\n";
		}
		qWarning() << "There are rejected images :" << rejectedImages;
		QMessageBox dialog(QMessageBox::Icon::Warning,
				   "Cannot open the image",
				   "The following images cannot be opened (since images with same name exist) :\n"
				   +str,
				   QMessageBox::StandardButton::Ok,
				   this);
	}
}

void NoiseModelWidget::onRemoveImageClicked() {
	for(QListWidgetItem *item: m_openImages->selectedItems()) {
		Q_ASSERT(imageData.images.erase(item->text()) > 0);
		delete item;
		updatePlotAndImage();
	}
}

void NoiseModelWidget::onListClick(QListWidgetItem *current, QListWidgetItem *previous) {
	this->updatePlotAndImage(current->text());
}

void NoiseModelWidget::closeAsked() {
	close();
}
