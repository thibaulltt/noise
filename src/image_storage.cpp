#include "../include/image_storage.h"

#include <cmath>
#include <cstdlib>
#include <limits>

double generateRandomGaussianValue(const nDis_t& noiseDistrib) {
	const double& mu = noiseDistrib.mu;
	const double& sigma = noiseDistrib.sigma;

	static const double epsilon = std::numeric_limits<double>::min();
	static const double two_pi = 2.0*M_PI;

	thread_local double z1;
	thread_local bool generate;
	generate = !generate;

	if (!generate)
		return z1 * sigma + mu;

	double u1, u2;
	do {
		u1 = rand() * (1.0 / RAND_MAX);
		u2 = rand() * (1.0 / RAND_MAX);
	}
	while ( u1 <= epsilon );

	double z0;
	z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
	z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
	return z0 * sigma + mu;
}

QVector<nDis_t> getNoiseDistributionForImage(const QVector<std::map<int,size_t>>& histos) {
	QVector<nDis_t> noiseModels;
	noiseModels.resize(histos.size());
	noiseModels.resize(histos.size());
	// for each component :
	for (int i = 0; i < histos.size(); ++i) {
		const std::map<int,size_t>&histo = histos[i];
		// get default values, for reference
		int max_idx = histos[i].begin()->first;
		size_t max = histos[i].begin()->second;
		// find the max value in the array
		for (const auto& keyValPair : histo) {
			if (keyValPair.second > max) {
				max = keyValPair.second;
				max_idx = keyValPair.first;
			}
		}
		// we have the mean (max val in occurence count)
		noiseModels[i].mu = static_cast<double>(max_idx);
		// compute the standard deviation :
		double sum = .0;
		size_t fullOccurences = 0;
		for (const auto& keyValPair : histo) {
			sum += static_cast<double>(keyValPair.second) * std::pow(( static_cast<double>(keyValPair.first) - noiseModels[i].mu ), 2.0);
			fullOccurences += keyValPair.second;
		}
		// compute sigma here :
		noiseModels[i].sigma = std::sqrt( sum / static_cast<double>(fullOccurences) );
	}
	return noiseModels;
}

image_database::image_database() {
	this->images.clear();
	this->generalDistribution.clear();
}

QVector<nDis_t> image_database::getNoiseModel() {
	return this->generalDistribution;
}

void image_database::addImage(const QString& name, const image& original, const image& denoised, const QVector<std::map<int,size_t>>& histos) {
	// generate noise distribution
	QVector<nDis_t> noiseDistributionForImage = getNoiseDistributionForImage(histos);
	// generate newimage storage struct
	image_storage newimagetostore{original, denoised, histos, noiseDistributionForImage};
	// add it to the images
	this->images.insert(std::make_pair(name, newimagetostore));
}

QVector<nDis_t> image_database::computeNoiseModel() {
	QVector<double> general_mu;
	QVector<double> general_sigma;
	// check if we have images in the database :
	if (this->images.size() != 0) {
		// since we assume each image is the same depth, resize mus and sigmas accordingly :
		general_mu.resize(this->images.begin()->second.noiseDistribution.size());
		general_sigma.resize(this->images.begin()->second.noiseDistribution.size());
		// Sum up all values of mu and sigma
		for (const auto& image : this->images) {
			for (int i = 0; i < image.second.noiseDistribution.size(); ++i) {
				const nDis_t& nd = image.second.noiseDistribution[i];
				general_mu[i] += nd.mu;
				general_sigma[i] += nd.sigma;
			}
		}
		// Make it a mean value
		for (int i = 0; i < general_mu.size(); ++i) {
			general_mu[i] /= static_cast<double>(this->images.size());
			general_sigma[i] /= static_cast<double>(this->images.size());
		}
		// return it :
		QVector<nDis_t> result;
		for (int i = 0; i < general_mu.size(); ++i) {
			nDis_t newNoiseDist{general_mu[i], general_sigma[i]};
			result.push_back(newNoiseDist);
		}
		this->generalDistribution = result;
		return result;
	} else {
		// return empty distribution
		std::cerr << "There were no images in the database." <<
			     "Returning an empty distribution." << std::endl;
		QVector<nDis_t> result;
		nDis_t emptynoisedist{.0,.0};
		result.push_back(emptynoisedist);
		return result;
	}
}

bool image_database::contains(QString name) {
	return this->images.count(name) != 0;
}
