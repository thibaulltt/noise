#include "../include/image.hpp"
#include <iostream>
#include <cmath>

#include <QImage>
#include <QtDebug>
#include <QtGlobal>

image::image() {}

image::image(std::string image_path) {
	this->load(image_path.c_str());
}

QColor image::at(int x, int y) const {
	if (x < 0) { x = std::abs(x)-1; }
	if (y < 0) { y = std::abs(y)-1; }
	if (x >= this->width()) { x = 2 * this->width() - x - 1; }
	if (y >= this->height()) { y = 2 * this->height() - y - 1; }
	return this->pixelColor(x,y);
}

void image::apply_gaussian_blur(size_t kernel_width) {
	if (kernel_width < 3) {kernel_width = 3;}
	if (kernel_width % 2 == 0) {kernel_width++;}

	double flat_kernel_weight = 1.0/static_cast<double>(kernel_width*kernel_width);

	// create weight kernel :
	QVector<QVector<double>> weight_kernel;
	weight_kernel.resize(static_cast<int>(kernel_width));

	for (int i = 0; i < static_cast<int>(kernel_width); ++i) {
		weight_kernel[i].resize(static_cast<int>(kernel_width));
		for (int j = 0; j < static_cast<int>(kernel_width); ++j) {
			weight_kernel[i][j] = flat_kernel_weight;
		}
	}

	image backup_image = image(*this);

	for (int y = 0; y < this->height(); ++y) {
		for (int x = 0; x < this->width(); ++x) {
			this->apply_blur_kernel(x,y,weight_kernel, &backup_image);
		}
	}

	return;
}

image image::compare_to_image(const image &i) const {
	image comparison_image = image(*this);
	comparison_image.fill(Qt::GlobalColor::black);

	for (int x = 0; x < this->width(); ++x) {
		for (int y = 0; y < this->height(); ++y) {
			int ir,ig,ib,r,g,b;
			i.at(x,y).getRgb(&ir,&ig,&ib);
			this->at(x,y).getRgb(&r,&g,&b);
			int cr = (ir > r) ? ir-r : r-ir;
			int cg = (ig > g) ? ig-g : g-ig;
			int cb = (ib > b) ? ib-b : b-ib;
			comparison_image.setPixelColor(x,y,QColor(cr,cg,cb));
		}
	}

	return image(i);
}

QVector<std::map<int, size_t>> image::diff_histo(const image& i) const {
	// Channels
	int planeNb;
	int maxVal;
	this->get_color_count_and_depth(&planeNb, &maxVal);
	Q_ASSERT(0 < planeNb && planeNb <= 3);

	QVector<std::map<int, size_t>> histos(planeNb);

	for(int y = 0; y < this->height(); ++y) {
		for(int x = 0; x < this->width(); ++x) {
			int ir, ig, ib, r, g, b;
			i.at(x, y).getRgb(&ir, &ig, &ib);
			this->at(x, y).getRgb(&r, &g, &b);
			QVector<int> diffs = {
				ir - r,
				ig - g,
				ib - b
			};

			for(int i(0); i < planeNb; ++i) {
				auto it(histos[i].find(diffs[i]));
				if(it == histos[i].end()) {
					histos[i].insert(std::make_pair(diffs[i], 1));
				}
				else {
					++histos[i][diffs[i]];
				}
			}
		}
	}

	return histos;
}

void image::apply_blur_kernel(int x, int y, const QVector<QVector<double>>& weights, const image* ref_img) {
	int offset = weights.size()/2;

	qreal r,g,b;
	qreal fr = .0, fb = .0, fg = .0;
	for (int ix = 0; ix < weights.size(); ++ix) {
		for (int iy = 0; iy < weights.size(); ++iy) {
			ref_img->at(x-offset+ix, y-offset+iy).getRgbF(&r,&g,&b);
			fr += r *weights[ix][iy]; fg += g * weights[ix][iy]; fb += b * weights[ix][iy];
		}
	}

	this->setPixelColor(x, y, QColor(static_cast<int>(fr * 255.0),static_cast<int>(fg * 255.0),static_cast<int>(fb * 255.0)));

	return;
}

QVector<QVector<size_t>> image::generate_histogram() const {
	QVector<QVector<size_t>> histogram;
	int max_val = 256;
	int nb_planes = 3;
	this->get_color_count_and_depth(&nb_planes, &max_val);
//	if (this->isGrayscale()) {
//		max_val = this->colorCount();
//	}
	histogram.resize(nb_planes);
	for (int i = 0; i < nb_planes; ++i) {
		histogram[i].fill(0, max_val);
	}
	int vals[3];
	for (int y = 0; y < this->height(); ++y) {
		for (int x = 0; x < this->width(); ++x) {
			this->pixelColor(x,y).getRgb(&vals[0], &vals[1], &vals[2]);
			for (int i = 0; i < nb_planes; ++i) {
				histogram[i][vals[i]] += 1;
			}
		}
	}
	return histogram;
}

void image::get_color_count_and_depth(int *number_of_planes, int *max_value_for_plane) const {
	QImage::Format image_format = this->format();
	switch (image_format) {
		case QImage::Format::Format_RGB16:
		case QImage::Format::Format_ARGB8565_Premultiplied:
			std::cout << "QImage::Format::{Format_RGB16|Format_ARGB8565_Premultiplied} : " << std::endl;
			std::cout << "This image format is completely stupid. As such, you will not have any histogram. Good luck." << std::endl;
			std::cout << "(Setting both values to 0)" << std::endl;
		case QImage::Format::Format_Invalid:
		case QImage::Format::Format_Mono:
		case QImage::Format::Format_MonoLSB:
			*number_of_planes = -1;
			*max_value_for_plane = -1;
			return;
		case QImage::Format::Format_Indexed8:
			*number_of_planes = 1;
			*max_value_for_plane = 256;
			return;
		case QImage::Format::Format_RGB32:
		case QImage::Format::Format_ARGB32:
		case QImage::Format::Format_ARGB32_Premultiplied:
			*number_of_planes = 3;
			*max_value_for_plane = 256;
			return;
		case QImage::Format::Format_RGB666:
		case QImage::Format::Format_ARGB6666_Premultiplied:
			*number_of_planes = 3;
			*max_value_for_plane = 64;
			return;
		case QImage::Format::Format_RGB555:
		case QImage::Format::Format_ARGB8555_Premultiplied:
			*number_of_planes = 3;
			*max_value_for_plane = 32;
			return;
		case QImage::Format::Format_RGB888:
			*number_of_planes = 3;
			*max_value_for_plane = 256;
			return;
		case QImage::Format::Format_RGB444:
		case QImage::Format::Format_ARGB4444_Premultiplied:
			*number_of_planes = 3;
			*max_value_for_plane = 16;
			return;
		case QImage::Format::Format_RGBX8888:
		case QImage::Format::Format_RGBA8888:
		case QImage::Format::Format_RGBA8888_Premultiplied:
			*number_of_planes = 3;
			*max_value_for_plane = 256;
			return;
		case QImage::Format::Format_BGR30:
		case QImage::Format::Format_RGB30:
		case QImage::Format::Format_A2BGR30_Premultiplied:
		case QImage::Format::Format_A2RGB30_Premultiplied:
			*number_of_planes = 3;
			*max_value_for_plane = 1024;
			return;
		case QImage::Format::Format_Alpha8:
			*number_of_planes = 1;
			*max_value_for_plane = 256;
			return;
		case QImage::Format::Format_Grayscale8:
			*number_of_planes = 1;
			*max_value_for_plane = 256;
			return;
#if QT_VERSION >= QT_VERSION_CHECK(5, 13, 0)
		case QImage::Format::Format_Grayscale16:
			*number_of_planes = 1;
			*max_value_for_plane = 65536;
			return;
#endif
		case QImage::Format::Format_RGBX64:
		case QImage::Format::Format_RGBA64:
		case QImage::Format::Format_RGBA64_Premultiplied:
			*number_of_planes = 3;
			*max_value_for_plane = 65536;
			return;
		default:
			// For unknown enums -> no planes, no values ( cannot compute the max val, or nb of planes )
			std::cout << "Ennum was not recognized ! Could not get planes and max vals." << std::endl;
			*number_of_planes = 0;
			*max_value_for_plane = 0;
			return;
	}
}

float image::RMSE(const image& ref) const {
    Q_ASSERT(ref.width() == width() && ref.height() == height());

    int channels, dummy;
    get_color_count_and_depth(&channels, &dummy);
    Q_ASSERT(channels == 1 || channels == 3);

    int pixelNb = width() * height();

    if(channels == 1) {
        float sum = 0.f;
        for(int y(0); y < height(); ++y) {
            for(int x(0); x < width(); ++x) {
                float diff = static_cast<float>(this->at(x, y).red() - ref.at(x, y).red());
                sum += diff * diff;
            }
        }
        return sum / pixelNb;
    }
    else {
        float sum_r = 0.f;
        float sum_g = 0.f;
        float sum_b = 0.f;
        for(int y(0); y < height(); ++y) {
            for(int x(0); x < width(); ++x) {
                float diff_r = static_cast<float>(this->at(x, y).red() - ref.at(x, y).red());
                float diff_g = static_cast<float>(this->at(x, y).green() - ref.at(x, y).green());
                float diff_b = static_cast<float>(this->at(x, y).blue() - ref.at(x, y).blue());
                sum_r += diff_r * diff_r;
                sum_g += diff_g * diff_g;
                sum_b += diff_b * diff_b;
            }
        }
        sum_r /= pixelNb;
        sum_g /= pixelNb;
        sum_b /= pixelNb;

        return (sum_r + sum_g + sum_b) / 3.0f;
    }
}

float image::PSNR(const image &ref) const {
    int channels, dummy;
    get_color_count_and_depth(&channels, &dummy);

    float rmse(RMSE(ref));

    float d = 255.f;

    return 10.f * std::log10((d * d) / rmse);
}
