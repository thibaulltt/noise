#include "../include/denoisewidget.hpp"
#include "../include/moc_denoisewidget.cpp"

#include "../include/imagewidget.hpp"
#include "../include/applicationdata.h"
#include "../include/libdenoising.h"

#include <QVBoxLayout>
#include <QShortcut>
#include <QtDebug>

DenoiseWidget::DenoiseWidget(const image &baseImage,
						 MainWindow *mainWindow,
						 QWidget *parent) :
	QWidget(parent),
	m_baseImage(baseImage),
	m_mainWindow(mainWindow),
    m_sigma(10.f)
{
	QVBoxLayout *layout(new QVBoxLayout(this));
	setLayout(layout);

    m_imageWidget = new FigureWidget("Image débruitée",
				   new ImageWidget(m_baseImage),
				   this);

	m_slider = new QSlider(Qt::Horizontal, this);
	m_slider->setRange(0, 100);
    m_slider->setValue(10);
    connect(m_slider, SIGNAL(valueChanged(int)), this, SLOT(setSigma(int)));

	m_validateButton = new QPushButton("Valider", this);
    connect(m_validateButton, SIGNAL(clicked()), this, SLOT(denoiseImage()));

	this->setWindowTitle("Qt : Modèle de bruit");

	layout->addWidget(m_imageWidget);
	layout->addWidget(m_slider);
	layout->addWidget(m_validateButton);

	QShortcut *shortcut = new QShortcut(QKeySequence(Qt::Key::Key_Escape), this, SLOT(closeAsked()));
	Q_UNUSED(shortcut)

	setAttribute(Qt::WA_DeleteOnClose);
}

void DenoiseWidget::closeAsked() {
	close();
}

void DenoiseWidget::setSigma(int sigma) {
    m_sigma = static_cast<float>(sigma);
}

void DenoiseWidget::denoiseImage() {
    image resImage(nlmeans_ipol(m_sigma, m_baseImage));
	m_imageWidget->setImage(resImage);
	ApplicationData::get()->setWorkImage(resImage);
}
