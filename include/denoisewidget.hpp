#ifndef DENOISEWIDGET_HPP
#define DENOISEWIDGET_HPP

#include "figurewidget.hpp"
#include "mainwindow.hpp"

#include <QWidget>
#include <QSlider>
#include <QPushButton>

class DenoiseWidget : public QWidget
{
	Q_OBJECT
public:
	DenoiseWidget(const image &baseImage,
				MainWindow *mainWindow = nullptr,
				QWidget *parent = nullptr);

signals:

public slots:
	void closeAsked();

private slots:
	void setSigma(int sigma);
	void denoiseImage();

private:
	FigureWidget *m_imageWidget;
	const image m_baseImage;
	QSlider *m_slider;
	QPushButton *m_validateButton;
	MainWindow *m_mainWindow;
	float m_sigma;


};

#endif // NOISEWIDGET_HPP
