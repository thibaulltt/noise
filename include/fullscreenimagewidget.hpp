#ifndef FULLSCREENIMAGEWIDGET_HPP
#define FULLSCREENIMAGEWIDGET_HPP

#include "imagewidget.hpp"
#include "./image.hpp"
#include <QDialog>
#include <QPushButton>

class FullscreenImageWidget: public QDialog {
	public:
		FullscreenImageWidget(const image &img, QWidget *parent = nullptr);

		//protected:
		virtual int exec() override;
	private:

		ImageWidget *m_imageWidget;
		QPushButton *m_closeButton;
};

#endif // FULLSCREENIMAGEWIDGET_HPP
