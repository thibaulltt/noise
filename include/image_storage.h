#ifndef IMAGE_STORAGE_H
#define IMAGE_STORAGE_H

// C++ STD #includes
#include <iostream>
#include <vector>
#include <map>

// Qt #includes
#include <QVector>

// User-defined #includes
#include "image.hpp"

typedef struct noiseDistributionForImage {
	double mu;
	double sigma;
} nDis_t ;

/**
 * @brief Generates a random value based on the gaussian distribution given by the noiseDistrib parameter.
 * @note The algorithm used here is the Box-Mueller algorithm.
 * @param noiseDistrib The noise distribution to generate values from.
 * @return A random value, sampled in the noiseDistrib gaussian distribution.
 */
double generateRandomGaussianValue(const nDis_t& noiseDistrib);

/**
 * @brief Gets the noise distribution of the given histogram (taken from an image)
 * @param histo The histogram(s) to analyse
 * @return The noise distribution of all channels
 */
QVector<nDis_t> getNoiseDistributionForImage(const QVector<std::map<int,size_t>>& histo);

/**
 * @brief Storage for a denoised image
 * @details The image_storage struct holds an image,
 * it's blurred counterpart, it's histogram of
 * differences and it's noise distribution.
 */
struct image_storage {
	/**
	 * @brief Original image.
	 */
	image originalImage;
	/**
	 * @brief Denoised image, obtained from blurring or applying NLMeans on the image.
	 */
	image blurredImage;
	/**
	 * @brief Histogram of differences from the original image to the denoised one.
	 */
	QVector<std::map<int, size_t>> histos;
	/**
	 * @brief Noise distribution on all channels of the image.
	 */
	QVector<nDis_t> noiseDistribution;
};

class image_database {
	public:
		/**
		 * @brief Images in the database.
		 * @details Contains the following data, for each image :
		 * <ul>
		 *	<li>The image</li>
		 *	<li>The denoised image</li>
		 *	<li>The histogram of differences between the images</li>
		 *	<li>The noise model of each channel of the image</li>
		 * </ul>
		 */
		std::map<QString, image_storage> images;
		/**
		 * @brief The general noise distribution of the image.
		 */
		QVector<nDis_t> generalDistribution;
	public:
		/**
		 * @brief Creates the image database, with nothing in it.
		 */
		image_database();
		/**
		 * @brief Returns and (if necessary) computes the noise model.
		 * @return The noise model of all images in this database
		 */
		QVector<nDis_t> getNoiseModel();
		/**
		 * @brief Adds an image to the database, by providing an image in input.
		 * @note Builds the noise distribution of the image when called.
		 */
		void addImage(const QString&, const image&, const image&, const QVector<std::map<int,size_t>>&);
		/**
		 * @brief Computes the noise model, given the images in the database.
		 * @return The noise model of all images in this database
		 */
		QVector<nDis_t> computeNoiseModel();
		/**
		 * @brief Check if the database contains a given key
		 * @return true if the database contains the key, false if not
		 */
		bool contains(QString);

//	protected slots:
};

extern image_database imageData;

#endif // IMAGE_STORAGE_H
