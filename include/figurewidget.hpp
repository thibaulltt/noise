#ifndef FIGUREWIDGET_HPP
#define FIGUREWIDGET_HPP

#include "../include/imagewidget.hpp"
#include "../include/image.hpp"

#include <QWidget>
#include <QLabel>

class FigureWidget: public QWidget
{
	public:
		FigureWidget(const QString &description = "",
			     ImageWidget *imageWidget = nullptr,
			     QWidget *parent = nullptr);
		void setImage(const image& image);
		const image &getImage() const;

	private:
		QLabel *m_description;
		ImageWidget *m_imageWidget;
};

#endif // FIGUREWIDGET_HPP
