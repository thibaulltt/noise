#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <string>

#include <QImage>
#include <QColor>
#include <QVector>
#include <map>

class image : public QImage {
	public:
		image();
		/**
		 * @brief Default image constructor. Defines an empty image.
		 */
		image(std::string image_path);
		/**
		 * @brief Returns the pixel at coordinates (X,Y).
		 * @details If X/Y are too big/too small for the image, we will compute the distance
		 * from the upper bound of width/length and move backwards on the line/column to get
		 * to a pixel in the image, as it the image was mirrored on each border.
		 * @param x X coordinate requested
		 * @param y Y coordinate requested
		 * @return A quadruplet of the format `QColor::AARRGGBB`, like the pixel() function.
		 * @see QImage::pixel()
		 */
		QColor at(int x, int y) const;
		/**
		 * @brief Applies a gaussian blur on the image, using a kernel of size `kernel_width`.
		 * @details If the kernel size provided is even, this function will take the next odd number as a kernel size.
		 * @warning In it's current state, the program will only generate mean kernels, not *actual* gaussian kernels
		 * @param kernel_width Size of the kernel to apply a gaussian blur.
		 */
		void apply_gaussian_blur(size_t kernel_width = 3);
		/**
		 * @brief Compares the current image with a given image, returning the delta as an image.
		 * @param i image to compare with
		 * @return A "delta" image.
		 */
		image compare_to_image(const image& i) const;

		/**
		 * @brief diff_for_noise_image
		 * @param i
		 * @return a difference image which values goes to [0, 255[ (representing [-127; 127] values
		 */
		QVector<std::map<int, size_t>> diff_histo(const image& i) const;
		/**
		 * @brief Genarates an histogram of the image.
		 * @warning Currently, the function only supports greyscale images and color images of per-pixel values up to 256. No 10-bit HDR pics here.
		 * @return A vector of vectors of value frequencies.
		 */
		QVector<QVector<size_t>> generate_histogram() const;
		/**
		 * @brief Gets the number of components for the image (3 if RGB, 1 if greyscale ...) and the max value for each component.
		 * @warning Computed by hand. There might be some errors.
		 * @param number_of_planes Pointer to value for storing the number of planes of the image
		 * @param max_value_for_plane Pointer to value for max available value for a plane
		 */
		void get_color_count_and_depth(int* number_of_planes, int* max_value_for_plane) const;

		/**
		 * @brief RMSE
		 * @param ref the ref image considered as the truth
		 * @return the root mean square for this image.
		 */
		float RMSE(const image& ref) const;

		/**
		 * @brief RMSE
		 * @param ref the ref image considered as the truth
		 * @return the Peak Signal to Noise Ratio for this image.
		 */
		float PSNR(const image& ref) const;

	protected:
		/**
		 * @brief Computes the given blur on one image, providing a weighting kernel to do it.
		 * @param x The X coordinate of the pixel to blur
		 * @param y The X coordinate of the pixel to blur
		 * @param ref_img Reference image to get pixel values from
		 * @param weights The weight matrix to compute the blur
		 */
		void apply_blur_kernel(int x, int y, const QVector<QVector<double>>& weights, const image* ref_img);
};

#endif // IMAGE_HPP
