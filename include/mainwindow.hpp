#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QPushButton>

#include "imagewidget.hpp"
#include "plotwidget.hpp"
#include "figurewidget.hpp"

class MainWindow : public QMainWindow {
		Q_OBJECT
	public:
		MainWindow(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
		virtual ~MainWindow();

	protected:
		QWidget *m_centralWidget;
		FigureWidget* m_groundTruth;
		FigureWidget* m_image;

		QWidget *m_buttonsWidget;
		QPushButton *m_noiseModelButton;
		QPushButton *m_noiseImageButton;
		QPushButton *m_denoiseImageButton;
		QPushButton *m_metricsButton;
		QPushButton *m_resetButton;

		void closeEvent(QCloseEvent *ev) override;

	signals:
		void closeAll();

	private slots:
		void openImage();
		void saveImage();
		void openNoiseWidget();
		void openDenoiseWidget();
		void openNoiseModelWidget();
		void openMetricsWidget();
		void resetWorkImage();
		void closeAsked();
		void finishedNoiseModel();


	public slots:
		void onGroundTruthChanged();
		void onWorkImageChanged();
};

#endif // MAINWINDOW_HPP
