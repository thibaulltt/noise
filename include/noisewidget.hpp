#ifndef NOISEWIDGET_HPP
#define NOISEWIDGET_HPP

#include "figurewidget.hpp"
#include "mainwindow.hpp"
#include "image_storage.h"

#include <QWidget>
#include <QSlider>
#include <QPushButton>

class NoiseWidget : public QWidget
{
		Q_OBJECT
	public:
		NoiseWidget(const image &baseImage,
			    MainWindow *mainWindow = nullptr,
			    QWidget *parent = nullptr);

	signals:

	public slots:
		void closeAsked();

	private slots:
		void setFactor(int factor);
		void noiseImage();
		void onNoiseValuesChange();

	private:
		FigureWidget *m_imageWidget;
		const image m_baseImage;
		QSlider *m_slider;
		QPushButton *m_validateButton;
		MainWindow *m_mainWindow;
		float m_factor;


};

#endif // NOISEWIDGET_HPP
