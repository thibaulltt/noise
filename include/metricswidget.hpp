#ifndef METRICSWIDGET_HPP
#define METRICSWIDGET_HPP

#include "figurewidget.hpp"
#include "mainwindow.hpp"
#include "image.hpp"
#include "image_storage.h"

#include <QWidget>
#include <QLabel>

class MetricsWidget : public QWidget
{
		Q_OBJECT
	public:
		MetricsWidget(QWidget *parent = nullptr);

	signals:

	public slots:
		void closeAsked();
		void onWorkImageChanged();
		void onGroundTruthChanged();
	private slots:

	private:
		void recomputeMetrics();

		QLabel *m_psnrLabel;
		QLabel *m_psnrValue;
		QLabel *m_noiseModelLabel;
		QLabel *m_muValue;
		QLabel *m_sigmaValue;

		MainWindow *m_mainWindow;

};

#endif // NOISEWIDGET_HPP
