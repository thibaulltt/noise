#ifndef PLOTWIDGET_HPP
#define PLOTWIDGET_HPP

#include <QWidget>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>

QT_CHARTS_USE_NAMESPACE

class PlotWidget : public QChartView {
		Q_OBJECT
	public:
		PlotWidget(const QVector<QVector<QPointF>> &seriess = {}, const QString& title = "", QWidget *parent = nullptr);
		virtual ~PlotWidget();

		int seriesCount() const;
		int addSerie(const QString &name = "");
		int addSerie(const QString &name, const QColor &color);
		void addPointToSerie(int serieIndex, const QPointF &point);
		void plotVector(QVector<QPointF>& series, QString name = "vector_data", const QColor& color = Qt::GlobalColor::black);
		template <typename T> void plotVector(QVector<T>& series, QString name = "vector_data", const QColor& color = Qt::GlobalColor::black);
		void clear();

	protected:
		QVector<QLineSeries*> m_series;
		QChart *m_chart;
};

#endif // PLOTWIDGET_HPP
