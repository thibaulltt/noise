#ifndef APPLICATIONDATA_H
#define APPLICATIONDATA_H

#include "../include/image.hpp"

#include <QVector>
#include <QObject>

class ApplicationData: public QObject {
    Q_OBJECT
public:
    static ApplicationData *get();

    void setNoiseValues(const QVector<QVector<int>> &NoiseValues);
    const QVector<QVector<int>> &noiseValues() const;

    void setWorkImage(const image &img);
    const image &workImage() const;

    void setGroundTruth(const image &img);
    const image &groundTruth() const;
signals:
    void workImageChanged();
    void groundTruthChanged();
    void noiseValuesChanged();

private:
    ApplicationData();

    QVector<QVector<int>> m_noiseValues;
    image m_workImage;
    image m_groundTruth;
};

#endif // APPLICATIONDATA_H
