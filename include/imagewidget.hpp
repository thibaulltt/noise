#ifndef IMAGEWIDGET_HPP
#define IMAGEWIDGET_HPP

#include <QWidget>
#include <QImage>
#include <QPixmap>
#include <QLabel>

#include "./image.hpp"

class ImageWidget : public QLabel {
	public:
		ImageWidget(QWidget *parent = nullptr);
		ImageWidget(const image &base_image, QWidget *parent = nullptr);
		virtual ~ImageWidget();

		void setImage(const image &base_image);
		void clearImage();
		const image &getImage() const;
		void setFullScreenPopupOnClick(bool val = true);
	protected:
		/**
		 * @brief Shown image in the widget
		 */
		image m_image;
		QPixmap m_pixmap;

		bool m_pressed = false;
		bool m_fullScreenPopupOnClick = true;

		void init();

		void mousePressEvent(QMouseEvent *ev) override;
		void mouseReleaseEvent(QMouseEvent *ev) override;
		void resizeEvent(QResizeEvent *event) override;

		virtual void clickedFunction();
	private slots:
		/**
		 * @brief Called when the base image changed
		 */
		void changedImage();
		/**
		 * @brief Called when the base image is updated (added noise ...)
		 */
		void updatedImage();
};

#endif // IMAGEWIDGET_HPP
