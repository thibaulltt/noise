#ifndef NOISEMODELWIDGET_HPP
#define NOISEMODELWIDGET_HPP

#include "mainwindow.hpp"
#include "image.hpp"
#include "figurewidget.hpp"
#include "plotwidget.hpp"

#include <QWidget>
#include <QListWidget>
#include <QPushButton>
#include <QProgressDialog>

#include <map>

class NoiseModelWidget: public QWidget {
    Q_OBJECT
public:
	NoiseModelWidget(MainWindow *mainWindow, QWidget *parent = nullptr);

private:
	QWidget *m_leftPanel;
	QWidget *m_centerPanel;
	QWidget *m_rightPanel;
	QListWidget *m_openImages;
	QWidget *m_leftButtonArea;
	QPushButton *m_addImages;
	QPushButton *m_addSingleImages;
	QPushButton *m_removeImages;
	FigureWidget *m_regularImage;
	FigureWidget *m_denoisedImage;
	FigureWidget *m_noiseImage;
	PlotWidget *m_distribution;

	MainWindow *m_mainWindow;

//	std::map<QString, ImageData> m_pathToImage;

	void updatePlotAndImage(QString name = "");
	/**
	 * @brief generateNoiseValues
	 * @param histos_sum
	 * @return width * height values (for each channels) corresponding to the offset to add to each pixel within
	 * the image to noise.
	 */
	QVector<QVector<int>> generateNoiseValues(const QString&, const QVector<std::map<int, size_t>> &histos_sum) const;
	image noiseValuesToImage(const QString&, const QVector<QVector<int>> &noiseValues) const;
	void onAddImageClicked(QStringList& list_of_files);

public slots:
	void closeAsked();

private slots:
	/**
	 * @brief Triggered when the user clicks on 'Add Files'
	 */
	void onAddFileClicked();
	/**
	 * @brief Triggered when the user clicks on 'Add Folders'
	 */
	void onAddFolderClicked();
	void onRemoveImageClicked();
	void onListClick(QListWidgetItem *current, QListWidgetItem *previous);
};

#endif // NOISEMODELWIDGET_HPP
